// import supertest from 'supertest';
// import app from './app.js';
var supertest = require('supertest')
var app = require('../app.js')
var server = require('../app.js').server;

describe('tests basiques', () => {
  afterEach(async () => {
    try {
     server.close();
    } catch (error) {
      console.error(error)
      throw error;
    }
  });

  it('test home', async (done) => {
    const res = await supertest(app).get('/').expect(200);
    done();
  });
  it('test id incorrect', async (done) => {
    const res = await supertest(app).get('/product_info/delete/-1').expect(302);
    done();
  });
});