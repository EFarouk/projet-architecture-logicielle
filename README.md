# Gestion des stocks et des ventes

Description permettant d'installer l'API développée pour gérer le stock de produits ainsi que leur vente.

## Pour commencer

Ce guide vous permetta d'obtenir une copie du projet opérationnel sur votre machine locale à des fins de développement et de test.

### Fonctionnalitées
- Ajout d'un Produit 
- Modification d'un produit renseigné 
- Suppression un produit renseigné 
- Modification du Stock d’un produit 
- Enregistrement d'une vente 
- Historique des entrées et sorties du stock (détail ventes etc...)

### Bonus
- Gestions des logs
- Tests unitaires
- Gestions des variables d'environnements
- Dockerization

### Prerequis

- Installation de Node.js
- Installation de npm
- Installation de MySQL
- Installation de Docker

### Installation

Voici étape par étape, l'installation du projet sur votre machine.

Récupérer le dossier 'projet-architecture-logicielle' que vous placerez à l'endroit qui vous convient le plus.

```
 git clone https://{votre répertoire}/projet-architecture-logicielle.git
```

Puis déplacez vous dans le repertoire en question

```
cd ./projet-architecture-logicielle
```

Télécharger l'ensemble des dépendances

```
npm install
```

Installer dotenv pour les variables d'environnements

```
npm install dotenv --save
```

Ensuite créer une base de données MySQL avec pour nom "bdd_gestion" et insérez le script sql "bdd_gestion_stocks_ventes.sql".

Pour finir modifier le fichier .env avec vos propres réglages.


## Dockerisation 

Commencez par créer votre image avec la commande suivante :

```
docker build -t <nom> .
```

Puis lancez la avec :

```
docker run -p 3000:3080 -d <nom>
```


## Lancer les tests

Avant de lancer les tests, veuillez installer jest et supertest avec la commande suivante :

```
npm install --save-dev jest supertest
```

Pour lancer les tests :

```
npm test
```


## Démarrage

Si votre bdd est déjà créée, alimentée et démarrée pas la peine de réitérer l'étape
Lancez simplement la commande suivante : 

```
nodejs ./app.js
```

Allez à l'url : http://localhost:3000/ pour pouvoir tester l'application

## Construction du projet

Ce projet a été construit avec un module npm : 
* [Générateur express](https://www.npmjs.com/package/express-generator)

## Version

J'utilise git directement pour gérer les versions de ce projet.

## Auteur

* **Farouk ECHAJARI** - *projet-architecture-logicielle* - [EFarouk](https://gitlab.com/EFarouk/)

## Remerciements

Merci à Guillaume Perez mon professeur d'architecture logicielle qui nous à soumis ce projet.
