/*Module*/
var express = require('express');
var db = require('../db_config');
var app = express();
module.exports = app;


/*route*/

app.get('/create', function (req, res) {

	var data = {
		title: "Ajouter nouveau produit",
		base_url: db.base_url,
		errors: '',
	}
	res.render('view_add_product_info', data);

});
app.post('/insert_product_info', function (req, res) {

	/*Form validation Starts*/
	req.Validator.validate('nom', { required: true });
	req.Validator.validate('categorie', { required: true });
	req.Validator.validate('date_stock', { required: true });
	req.Validator.validate('qt_stock', { required: true });
	req.Validator.validate('price', { required: true });
	req.Validator.validate('date_debut', { required: true });
	req.Validator.validate('date_fin', { required: true });

	req.Validator.getErrors(function (errors) {

		if (errors.length > 0) {
			var data = {
				title: "Ajouter nouveau produit",
				base_url: db.base_url,
				errors: errors,
			}
			console.error(errors);
			res.render('view_add_product_info', data);
		} else {

			var insert_product_data = {
				'Nom': req.body.nom,
				'Categorie': req.body.categorie,
			}

			// insert products_info
			db.qb.insert('produit', insert_product_data, function (error, results) {
				db.qb.select_max('Id').get('produit', function (err, res) {
					var last_inserted_id = res[0]['Id'];

					var insert_prix_data = {
						'Id_produit': last_inserted_id,
						'Valeur': req.body.price,
						'Date_debut': req.body.date_debut,
						'Date_Fin': req.body.date_fin,
					}

					// insert in prix_data
					db.qb.insert('prix', insert_prix_data, function () { });

					var insert_stock_mv_data = {
						'Id_produit': last_inserted_id,
						'Date': req.body.date_stock,
						'Qt_mv': req.body.qt_stock,
					}

					db.qb.insert('stock_mv', insert_stock_mv_data, function () { });


					var insert_in_out_data = {
						'product_id': last_inserted_id,
						'in_qty': req.body.qt_stock,
						'out_qty': '0',
						'in_out_type': '1',
						'in_out_unt_price': req.body.price,
						'details': 'Nouveau produit',
					}

					// insert in products_in_out_data
					db.qb.insert('product_in_out_data', insert_in_out_data, function () { });

				});

			});

			res.redirect('/');
		}

	});
	/*Form validation End*/

});

app.get('/delete/:product_id', function (request, response) {

	var deleted_id = request.params.product_id;
	db.qb.delete('produit', { Id: deleted_id }, function (error, results) { });
	db.qb.delete('product_in_out_data', { product_id: deleted_id }, function (error, results) { });

	response.redirect('/');

});

app.get('/edit_product_info/:product_id', function (request, response) {

	var modify_id = request.params.product_id;
	db.qb.where('produit.Id', modify_id).join('prix', 'produit.Id=prix.Id_produit', 'left').join('stock_mv', 'produit.Id=stock_mv.Id_produit', 'right')
		.get('produit', function (error, results) {

			var data = {
				base_url: db.base_url,
				title: "Modifier produit | Acceuil",
				prodit_a_modifier: results,
			}

			response.render("view_modify_products", data);

		});
});

app.post('/edit_product_info', function (request, response) {

	/*Form validation Starts*/
	request.Validator.validate('nom', { required: true });
	request.Validator.validate('categorie', { required: true });
	request.Validator.validate('price', { required: true });
	request.Validator.validate('date_debut', { required: true });
	request.Validator.validate('date_fin', { required: true });
	request.Validator.validate('qt_stock', { required: true });

	request.Validator.getErrors(function (errors) {

		if (errors.length > 0) {
			var data = {
				title: "Modifier produit",
				base_url: db.base_url,
				errors: errors,
			}
			console.error(errors);
			response.render('view_modify_products', data);
		} else {

			var id_produit = request.body.id_produit;

			db.qb.where('Id_produit', id_produit).get('stock_mv', function (error, results) {
				var previous_stock = results[0]['Qt_mv'];

				var update_product_data = {
					'Nom': request.body.nom,
					'Categorie': request.body.categorie,
				}

				db.qb.update('produit', update_product_data, { 'Id': id_produit }, function (error, results) {
					if (errors.length > 0) {
						var data = {
							title: "Modifier produit",
							base_url: db.base_url,
							errors: errors,
						}
						console.error(errors);
						response.render('view_modify_products', data);
					} else {

						var update_prix_data = {
							'Valeur': request.body.price,
							'Date_debut': new Date(request.body.date_debut).toJSON().slice(0, 19).replace('T', ' '),
							'Date_Fin': new Date(request.body.date_fin).toJSON().slice(0, 19).replace('T', ' '),
					}
						

						db.qb.update('prix', update_prix_data, { 'Id_produit': id_produit }, function (error, results) {
							if (errors.length > 0) {
								var data = {
									title: "Modifier produit",
									base_url: db.base_url,
									errors: errors,
								}
								console.error(errors);
								response.render('view_modify_products', data);
							} else {

								var update_stock_mv_data = {
									'Qt_mv': request.body.qt_stock,
									'Date': new Date().toLocaleString(),
								}

								db.qb.update('stock_mv', update_stock_mv_data, { 'Id_produit': id_produit }, function (error, results) {
									if (errors.length > 0) {
										var data = {
											title: "Modifier produit",
											base_url: db.base_url,
											errors: errors,
										}
										console.error(errors);
										response.render('view_modify_products', data);
									} else {



										if (parseInt(request.body.qt_stock) > parseInt(previous_stock)) {

											var Qt_mv = parseFloat(request.body.qt_stock) - parseFloat(previous_stock);

											var insert_in_out_data = {
												'product_id': id_produit,
												'in_qty': Qt_mv,
												'out_qty': '0',
												'in_out_type': '1',
												'in_out_unt_price': request.body.price,
												'details': 'Modification produit',
											}

											// insert in products_in_out_data
											db.qb.insert('product_in_out_data', insert_in_out_data, function () { });
										} else if (parseInt(request.body.qt_stock) < parseInt(previous_stock)) {
											var Qt_mv = parseFloat(previous_stock) - parseFloat(request.body.qt_stock);


											var insert_in_out_data = {
												'product_id': id_produit,
												'in_qty': '0',
												'out_qty': Qt_mv,
												'in_out_type': '2',
												'in_out_unt_price': request.body.price,
												'details': 'Modification produit',
											}

											// insert in products_in_out_data
											db.qb.insert('product_in_out_data', insert_in_out_data, function () { });
										}

									}
								});

							}
						});

					}
				});

			});

			response.redirect('/');
		}

	});
	/*Form validation End*/

});

app.get('/in_history', function (request, response) {

	db.qb.where('in_out_type', 1).join('produit', 'produit.Id=product_in_out_data.product_id', 'left')
		.get('product_in_out_data', function (error, results) {

			var data = {
				title: "Historique des entrées produits",
				base_url: db.base_url,
				historique_produit: results,
			}
			response.render('view_product_in_out_history', data);
		});

});

app.get('/out_history', function (request, response) {

	db.qb.where('in_out_type', 2).join('produit', 'produit.Id=product_in_out_data.product_id', 'left')
		.get('product_in_out_data', function (error, results) {

			var data = {
				title: "Historique des sorties produits",
				base_url: db.base_url,
				historique_produit: results,
			}
			response.render('view_product_in_out_history', data);
		});

});

app.get('/modify_stock', function (request, response) {

	db.qb.get('produit', function (error, results) {

		var data = {
			title: "Historique des sorties produits",
			base_url: db.base_url,
			liste_produits: results,
		}
		response.render('view_modify_stock', data);
	});
});

app.post('/modify_stock', function (request, response) {

	var id_produit = request.body.id_produit;
	var qt_stock = request.body.qt_stock;
	var choix = request.body.choix;

	db.qb.where('Id_produit', id_produit).get('stock_mv', function (error, results) {
		var previous_stock = results[0]['Qt_mv'];

		db.qb.where('Id_produit', id_produit).get('prix', function (error, results) {
			var price = results[0]['Valeur'];

			if (choix === 'add') {

				var insert_in_out_data = {
					'product_id': id_produit,
					'in_qty': qt_stock,
					'out_qty': '0',
					'in_out_type': '1',
					'in_out_unt_price': price,
					'details': 'Ajout stock',
				}

				// insert in products_in_out_data
				db.qb.insert('product_in_out_data', insert_in_out_data, function (error, results) { });


				var update_data = {
					Qt_mv: parseFloat(previous_stock) + parseFloat(qt_stock),
				}

				db.qb.update('stock_mv', update_data, { 'Id_produit': id_produit }, function (error, results) { });
			} else {


				var insert_in_out_data = {
					'product_id': id_produit,
					'in_qty': '0',
					'out_qty': qt_stock,
					'in_out_type': '2',
					'in_out_unt_price': price,
					'details': 'Retrait stock',
				}

				// insert in products_in_out_data
				db.qb.insert('product_in_out_data', insert_in_out_data, function (error, results) { });


				var update_data = {
					Qt_mv: parseFloat(previous_stock) - parseFloat(qt_stock),
				}

				db.qb.update('stock_mv', update_data, { 'Id_produit': id_produit }, function (error, results) { });

			}

		});
	});

	response.redirect('/');

});

