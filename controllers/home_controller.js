/*Module*/
var express = require('express');
var db = require('../db_config');
var app = express();
module.exports = app;


/*route*/
app.get('/', function (request, response) {

	db.qb.join('prix', 'produit.Id=prix.Id_produit', 'left').join('stock_mv', 'produit.Id=stock_mv.Id_produit', 'right').get('produit', function (error, results) {

		var data = {
			base_url: db.base_url,
			title: "Liste du stock | Acceuil",
			liste_produits: results,
		}
		response.render("view_home", data);

	});
});

app.get('/home', function (request, response) {

	response.redirect('/');

});

