/*Module*/
var express = require('express');
var db = require('../db_config');
var app = express();
module.exports = app;


/*route*/

app.get('/sell', function (request, response) {


	db.qb.get('produit', function (error, results) {

		var data = {
			title: "Sell Product",
			base_url: db.base_url,
			liste_produits: results,
		}

		response.render('view_product_sell', data);
	});

});

app.post('/sell', function (request, response) {


	var id_produit = request.body.id_produit;
	var qt_stock = request.body.qt_stock;

	db.qb.where('Id_produit', id_produit).get('prix', function (error, results) {

		var price = results[0]['Valeur'];

		var insert_in_out_data = {
			'product_id': id_produit,
			'in_qty': '0',
			'out_qty': qt_stock,
			'in_out_type': '2',
			'in_out_unt_price': price,
			'details': 'Vente',
		}

		// insert in products_in_out_data
		db.qb.insert('product_in_out_data', insert_in_out_data, function (error, results) {

		});

		db.qb.where('Id_produit', id_produit).get('stock_mv', function (error, results) {
			var previous_stock = results[0]['Qt_mv'];

			var update_data = {
				Qt_mv: parseFloat(previous_stock) - parseFloat(qt_stock),
			}
			db.qb.update('stock_mv', update_data, { 'Id_produit': id_produit }, function (error, results) { });
		});
	});

	response.redirect('/');

});
