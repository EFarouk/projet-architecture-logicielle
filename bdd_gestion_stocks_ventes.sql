-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Jeu 06 Février 2020 à 05:45
-- Version du serveur :  5.6.20-log
-- Version de PHP :  7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `bdd_gestion`
--

-- --------------------------------------------------------

--
-- Structure de la table `prix`
--

CREATE TABLE IF NOT EXISTS `prix` (
`Id_prix` int(11) NOT NULL,
  `Id_produit` int(11) DEFAULT NULL,
  `Valeur` float DEFAULT NULL,
  `Date_debut` date DEFAULT NULL,
  `Date_Fin` date DEFAULT NULL
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

-- --------------------------------------------------------

--
-- Structure de la table `product_in_out_data`
--

CREATE TABLE IF NOT EXISTS `product_in_out_data` (
`product_in_out_data_id` int(11) NOT NULL,
  `product_id` int(11) DEFAULT NULL,
  `in_qty` double DEFAULT NULL,
  `out_qty` double DEFAULT NULL,
  `in_out_type` int(11) DEFAULT NULL COMMENT '1 for in,2 for Out',
  `in_out_unt_price` double DEFAULT NULL,
  `details` text,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=79 ;

-- --------------------------------------------------------

--
-- Structure de la table `produit`
--

CREATE TABLE IF NOT EXISTS `produit` (
`Id` int(11) NOT NULL,
  `Nom` varchar(255) DEFAULT NULL,
  `Categorie` varchar(255) DEFAULT NULL
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

-- --------------------------------------------------------

--
-- Structure de la table `produit_vente`
--

CREATE TABLE IF NOT EXISTS `produit_vente` (
`Id` int(11) NOT NULL,
  `Id_produit` int(11) DEFAULT NULL,
  `Id_ventes` int(11) DEFAULT NULL,
  `Quantite` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `stock_mv`
--

CREATE TABLE IF NOT EXISTS `stock_mv` (
`Id_stock_mv` int(11) NOT NULL,
  `Id_produit` int(11) DEFAULT NULL,
  `Date` date DEFAULT NULL,
  `Qt_mv` int(11) DEFAULT NULL
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

-- --------------------------------------------------------

--
-- Structure de la table `ventes`
--

CREATE TABLE IF NOT EXISTS `ventes` (
`Id` int(11) NOT NULL,
  `Date` date DEFAULT NULL,
  `Prix_totale` float DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Index pour les tables exportées
--

--
-- Index pour la table `prix`
--
ALTER TABLE `prix`
 ADD PRIMARY KEY (`Id_prix`), ADD KEY `Id_produit` (`Id_produit`);

--
-- Index pour la table `product_in_out_data`
--
ALTER TABLE `product_in_out_data`
 ADD PRIMARY KEY (`product_in_out_data_id`);

--
-- Index pour la table `produit`
--
ALTER TABLE `produit`
 ADD PRIMARY KEY (`Id`);

--
-- Index pour la table `produit_vente`
--
ALTER TABLE `produit_vente`
 ADD PRIMARY KEY (`Id`), ADD KEY `Id_produit` (`Id_produit`), ADD KEY `Id_ventes` (`Id_ventes`);

--
-- Index pour la table `stock_mv`
--
ALTER TABLE `stock_mv`
 ADD PRIMARY KEY (`Id_stock_mv`), ADD KEY `Id_produit` (`Id_produit`);

--
-- Index pour la table `ventes`
--
ALTER TABLE `ventes`
 ADD PRIMARY KEY (`Id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `prix`
--
ALTER TABLE `prix`
MODIFY `Id_prix` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT pour la table `product_in_out_data`
--
ALTER TABLE `product_in_out_data`
MODIFY `product_in_out_data_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=79;
--
-- AUTO_INCREMENT pour la table `produit`
--
ALTER TABLE `produit`
MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT pour la table `produit_vente`
--
ALTER TABLE `produit_vente`
MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `stock_mv`
--
ALTER TABLE `stock_mv`
MODIFY `Id_stock_mv` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT pour la table `ventes`
--
ALTER TABLE `ventes`
MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
