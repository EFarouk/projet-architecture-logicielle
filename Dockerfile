FROM node:12

WORKDIR /usr/src/app

ARG NODE_ENV
ENV NODE_ENV $NODE_ENV

# COPY package.json /usr/src/app/
COPY package*.json ./
RUN npm install

# COPY . /usr/src/app
COPY . .

ENV PORT 3000
EXPOSE $PORT
CMD [ "node", "./app.js" ]
