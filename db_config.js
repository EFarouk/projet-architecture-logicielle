require('dotenv').config();
/**
 * Constants
 */
const host_env = process.env.HOST;
const user_env = process.env.USER_MYSQL;
const password_env = process.env.PASSWORD_MYSQL;
const database_env = process.env.DB_NAME;
const port_env = process.env.PORT;

/*MySQL Database Connection*/
var mysql      = require('mysql');

var db_settings = {
    host     : host_env,
  	user     : user_env,
  	password : password_env,
  	database : database_env
};
var qb = require('node-querybuilder').QueryBuilder(db_settings, 'mysql', 'single');
 
module.exports.qb=qb;

/*Base Url*/
var base_url='http://'+host_env+':'+port_env+'/';
module.exports.base_url=base_url;
